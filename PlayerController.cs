using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    public int coins = 0, hearts = 3, score = 0;
    public float moveSpeed = 1.6f;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 initialPosition;
    private bool canMove = true, isRed = false;

    void Start()
    {
        initialPosition = transform.position;
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.color = Color.yellow;
    }

    private void Update()
    {
        if (canMove)
        {
            Vector3 newPosition = transform.position;
            newPosition.y = initialPosition.y;
            transform.position = newPosition;

            if (Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
            {
                float horizontalInput = Input.GetAxisRaw("Horizontal");
                float verticalInput = Input.GetAxisRaw("Vertical");

                if (horizontalInput != 0f)
                {
                    moveDirection = new Vector3(horizontalInput, 0f, 0f);
                }
                else if (verticalInput != 0f)
                {
                    moveDirection = new Vector3(0f, 0f, verticalInput);
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            transform.Translate(moveDirection.normalized * moveSpeed * Time.fixedDeltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            if (moveDirection != Vector3.zero)
            {
                moveDirection = -moveDirection;
            }
        }
    }
}
