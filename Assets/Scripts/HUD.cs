using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Text CoinsText;
    public Text ScoreText;
    public Text HeartsText;

    Pacman pacman;

    void Start()
    {
        GameObject pacmanObject = GameObject.Find("Pacman");
        if (pacmanObject != null)
        {
            pacman = pacmanObject.GetComponent<Pacman>();
        }
    }

    void Update()
    {
        if (pacman != null)
        {
            CoinsText.text = "Coins: " + pacman.coins.ToString() + "/360";
            ScoreText.text = "Score: " + pacman.score.ToString();
            if (pacman.hearts == 0)
            {
                HeartsText.color = Color.red;
                HeartsText.text = "Game Over";
            }
            else
            {
                HeartsText.text = "Hearts: " + pacman.hearts.ToString();
            }
            if(pacman.coins == 360)
            {
                HeartsText.color = Color.green;
                HeartsText.text = "Congrats";
                pacman.GetComponent<MeshRenderer>().enabled = false;
            }
                
        }
    }
}
