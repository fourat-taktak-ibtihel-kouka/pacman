using UnityEngine;

public class PacmanScript : MonoBehaviour
{
    public float speed = 5.0f;
    public int coins = 0, hearts = 3, score = 0;
    private Vector3 moveDirection = Vector3.zero;
    private Rigidbody rb;

    void Start()
    {
        // Set the initial move direction to forward.
        moveDirection = Vector3.forward;

        // Get the Rigidbody component.
        rb = GetComponent<Rigidbody>();

        // Freeze Pacman's rotation.
        rb.constraints = RigidbodyConstraints.FreezeRotation;
    }

    void Update()
    {
        // Get the current input state.
        Vector2 inputAxis = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        // If the player has pressed a button, change the move direction.
        if (Input.GetButtonDown("Right"))
        {
            moveDirection = Vector3.right;
        }
        else if (Input.GetButtonDown("Left"))
        {
            moveDirection = Vector3.left;
        }
        else if (Input.GetButtonDown("Up"))
        {
            moveDirection = Vector3.forward;
        }
        else if (Input.GetButtonDown("Down"))
        {
            moveDirection = Vector3.back;
        }

        // Move Pacman in the current move direction.
        rb.MovePosition(transform.position + moveDirection * speed * Time.deltaTime);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            moveDirection = Vector3.zero;
        }
        if (collision.gameObject.CompareTag("Coin"))
        {
            coins++;
            Destroy(collision.gameObject);
        }
    }
}
