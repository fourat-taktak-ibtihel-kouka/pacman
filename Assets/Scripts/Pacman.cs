using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Pacman : MonoBehaviour
{
    public int coins = 0, hearts = 3, score = 0;
    public float moveSpeed = 1.6f;
    public Material orange,yellow;
    private Vector3 moveDirection = Vector3.zero;
    private Rigidbody rb;
    private Vector3 PacInitialPosition, GhostInitialPosition;
    private bool canMove = true, isRed = false;

    private void Start()
    {
        PacInitialPosition = transform.position;
        rb = GetComponent<Rigidbody>();
        Renderer renderer = GetComponent<Renderer>();
        renderer.material = yellow;
    }

    private void Update()
    {
        if (canMove)
        {
            Vector3 newPosition = transform.position;
            newPosition.y = PacInitialPosition.y;
            transform.position = newPosition;

            float horizontalInput = Input.GetAxisRaw("Horizontal");
            float verticalInput = Input.GetAxisRaw("Vertical");

            if (horizontalInput != 0f)
            {
                moveDirection = new Vector3(horizontalInput, 0f, 0f);
            }
            else if (verticalInput != 0f)
            {
                moveDirection = new Vector3(0f, 0f, verticalInput);
            }
            rb.velocity = moveDirection.normalized * moveSpeed;

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            float horizontalInput = Input.GetAxisRaw("Horizontal");
            float verticalInput = Input.GetAxisRaw("Vertical");
            if (rb.velocity == Vector3.zero)
            {
                if (horizontalInput != 0f)
                {
                    moveDirection = new Vector3(-horizontalInput, 0f, 0f);
                }
                else if (verticalInput != 0f)
                {
                    moveDirection = new Vector3(0f, 0f, -verticalInput);
                }
            }
        }
        else if (collision.gameObject.CompareTag("Coin"))
        {
            Coins(collision);
        }
        else if (collision.gameObject.CompareTag("powerUp"))
        {
            StartCoroutine(Powerups(collision));
        }
        else if (collision.gameObject.CompareTag("Ghost"))
        {
            StartCoroutine(Ghosts(collision));
        }
    }

    private void Coins(Collision collision)
    {
        Destroy(collision.gameObject);
        coins++;
        score += 50;
    }

    private IEnumerator Powerups(Collision collision)
    {
        Destroy(collision.gameObject);
        score += 200;
        Renderer renderer = GetComponent<Renderer>();
        if (renderer != null)
        {
            isRed = true;
            renderer.material = orange;

            yield return new WaitForSeconds(10f);

            isRed = false;
            renderer.material = yellow;
        }
    }

    private IEnumerator Ghosts(Collision collision)
    {
        if (isRed)
        {
            Destroy(collision.gameObject);
            score += 500;
        }
        else if (!isRed && hearts > 1)
        {
            canMove = false;
            GetComponent<MeshRenderer>().enabled = false;
            hearts--;
            score -= 2000;
            if (score <= 0)
            {
                score = 0;
            }
            yield return new WaitForSeconds(2f);

            transform.position = PacInitialPosition;
            canMove = true;
            GetComponent<MeshRenderer>().enabled = true;
        }
        else if (hearts == 1)
        {
            canMove = false;
            GetComponent<MeshRenderer>().enabled = false;
            transform.position = PacInitialPosition;
            hearts--;
        }
    }

}


