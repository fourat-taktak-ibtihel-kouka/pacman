using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    public float moveSpeed = 0.5f;
    private Rigidbody rb;
    private Vector3 currentDirection, previousPosition;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        currentDirection = Vector3.back;
        previousPosition = transform.position;
    }

    void FixedUpdate()
    {
        if (transform.position != previousPosition)
        {
            previousPosition = transform.position;
        }
        else
        {
            currentDirection = GetNewRandomDirection();
        }
        rb.velocity = currentDirection * moveSpeed;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Coin") || collision.gameObject.CompareTag("powerUp"))
        {
            Physics.IgnoreCollision(rb.GetComponent<Collider>(), collision.collider);
        }
        else if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Ghost"))
        {
            currentDirection = GetNewRandomDirection();
        }

    }

    Vector3 GetNewRandomDirection()
    {
        int randomDirection = UnityEngine.Random.Range(0, 4);

        switch (randomDirection)
        {
            case 0:
                return Vector3.left;
            case 1:
                return Vector3.right;
            case 2:
                return Vector3.forward;
            case 3:
                return Vector3.back;
            default:
                return Vector3.back;
        }
    }
}

